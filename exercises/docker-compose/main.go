package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
)

const (
	RedisTTL      = 24 * time.Hour
	RedisViewsKey = "views"
)

var (
	rdb *redis.Client
	ctx = context.Background()
)

func handler(w http.ResponseWriter, r *http.Request) {
	value, err := rdb.Get(ctx, RedisViewsKey).Result()
	if err != nil {
		log.Fatal("redis get: ", err)
	}

	current, _ := strconv.Atoi(value)
	err = rdb.Set(ctx, RedisViewsKey, current+1, RedisTTL).Err()
	if err != nil {
		log.Panic("redis set: ", err)
	}

	log.Println("request completed")
	fmt.Fprintf(w, fmt.Sprintf("Views: %d", current+1))
}

func main() {
	// init redis
	rdb = redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})
	defer rdb.Close()

	// check redis connection
	pong, err := rdb.Ping(context.Background()).Result()
	if err != nil {
		log.Panic("redis ping: ", err)
	}
	log.Println(pong, err)

	// initial value for a counter
	err = rdb.Set(ctx, RedisViewsKey, 0, RedisTTL).Err()
	if err != nil {
		log.Panic("redis set: ", err)
	}

	http.HandleFunc("/", handler)
	log.Println("Running on http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
